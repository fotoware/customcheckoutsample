﻿namespace OrderDemo.Models
{
    public enum CardType
    {
        Visa,
        MasterCard,
        ACMECard
    }
}