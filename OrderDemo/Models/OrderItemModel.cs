﻿namespace OrderDemo.Models
{
    public class OrderItemModel
    {
        public string AssetHref { get; set; }
        public string PreviewHref { get; set; }
        public string RenditionName { get; set; }
        public string AssetTitle { get; set; }
        public string AssetDescription { get; set; }
    }
}