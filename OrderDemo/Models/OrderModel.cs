﻿using System.Collections.Generic;

namespace OrderDemo.Models
{
    public class OrderModel
    {
        public List<OrderItemModel> OrderItems { get; set; }
        public string OrderReference { get; set; }
        public string OrderHref { get; set; }
        public string CardNumber { get; set; }
    }
}