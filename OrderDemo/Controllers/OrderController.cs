﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Threading.Tasks;
using OrderDemo.Models;
using System.Text;
using System.IO;
using System;
using Newtonsoft.Json;
using System.Dynamic;

namespace OrderDemo.Controllers
{
    /*
     * To use this demo: 
     * In FotoWeb configuration, check "Custom checkout page", and enter http://localhost:61025/Order?orderHref={{orderhref}} in textbox.
     * Set ApiKey to the correct API key below
     * Set Host to the correct host below (host is protocol + hostname + port)
     */
    public class OrderController : Controller
    {
        string ApiKey = "<insert API key here>";
        string Host = "<insert host here>";
        OrderModel ViewModel;
        string OrderHref;

        /*
         * Index page handler. Navigating to http://localhost:61025/Order?orderHref={{orderhref}} will call this method, with orderHref as argument
         */
        public async Task<ActionResult> Index(string orderHref)
        {
            await LoadOrderData(orderHref);

            ViewBag.OrderHref = orderHref;
            return View();
        }


        /*
         * Send GET request to load the order data:
         * 
         * GET <host>/fotoweb/orders/c04c7c790e1a2353d6845201251a26e08ca14e6ef2f1008d266664486347de20
         * Accept: application/vnd.fotoware.order+json
         * FWAPIToken: aNObV42pr7E7rnY6cshS23Buja7kuQza
         * 
         */
        private async Task<ActionResult> LoadOrderData(string orderHref)
        {
            OrderHref = orderHref;
            ViewModel = new OrderModel();
            ViewModel.OrderItems = new List<OrderItemModel>();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(orderHref);
            request.Accept = @"application/vnd.fotoware.order+json";
            request.Headers["FWAPIToken"] = ApiKey;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream(), UTF8Encoding.UTF8))
                    {
                        string responseText = reader.ReadToEnd();
                        HandleLoadOrderDataRespone(responseText);
                    }
                }
            }
            catch (WebException ex)
            {
                var response = ex.Response as HttpWebResponse;
                WebResponse errorResponse = ex.Response;
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    ViewBag.StateError = "Unauthorized";
                }
                else if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    ViewBag.StateError = "NotFound";
                }
                else
                {
                    ViewBag.StateError = "UnknownError";
                }
            }

            return View(ViewModel);
        }

        /*
         * Handle response from GET <host>/fotoweb/orders/c04c7c790e1a2353d6845201251a26e08ca14e6ef2f1008d266664486347de20
         * argument responseText is the JSON response returned from FotoWeb
         */
        private void HandleLoadOrderDataRespone(string responseText)
        {
            dynamic responseJson = System.Web.Helpers.Json.Decode(responseText);

            TempData["originalOrder"] = responseJson;
            TempData["fullOrderHref"] = OrderHref;
            ViewModel.OrderReference = responseJson.orderReference;
            ViewModel.OrderHref = OrderHref;
            if (responseJson.state == "created")
            {
                DynamicJsonArray orderItems = responseJson.orderItems;
                for (var i = 0; i < orderItems.Count(); i++)
                {
                    OrderItemModel orderItem = LoadAssetData(Host + orderItems[i]["asset"]);
                    orderItem.RenditionName = orderItems[i].rendition.name;
                    ViewModel.OrderItems.Add(orderItem);
                }
                ViewBag.LoadStatus = "Success";
            }
            else
            {
                ViewBag.StateError = "NotInCreatedState";
            }
        }

        /*
         * Use the FotoWeb API to load each asset in the order
         * argument assetHref is the asset permalink
         */
        private OrderItemModel LoadAssetData(string assetHref)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(assetHref);
            request.Accept = @"application/vnd.fotoware.asset+json";
            request.Headers["FWAPIToken"] = ApiKey;

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (var reader = new System.IO.StreamReader(response.GetResponseStream(), UTF8Encoding.UTF8))
                    {
                        string responseText = reader.ReadToEnd();
                        dynamic responseJson = System.Web.Helpers.Json.Decode(responseText);
                        OrderItemModel orderItem = new OrderItemModel();
                        orderItem.AssetHref = assetHref;
                        orderItem.PreviewHref = Host + responseJson.previews[4].href;
                        orderItem.AssetTitle = responseJson.metadata["5"].value;
                        orderItem.AssetDescription = responseJson.metadata["120"].value;
                        return orderItem;
                    }
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
            }

            return null;
        }

        /*
         * Send SUBMIT request with order data to FotoWeb API:
         * SUBMIT <host>/fotoweb/orders/c04c7c790e1a2353d6845201251a26e08ca14e6ef2f1008d266664486347de20
         * Accept: application/vnd.fotoware.order+json
         * FWAPIToken: aNObV42pr7E7rnY6cshS23Buja7kuQza
         * 
         * Example content data (order with 1 asset with 4 custom fields): {
         *   "orderData":[],
         *   "adminComment":"",
         *   "expires":null,
         *   "orderItems":[{
         *     "itemData":[
         *       {"id":"2d840ba6-5ad6-41ce-a016-573331a59e17","value":"Print"},
         *       {"id":"b9393af5-44cf-4dd6-a815-e8477888888c","value":"11"},
         *       {"id":"783555e5-ce6c-4e50-9fe9-95fd7c791008","value":"2"},
         *       {"id":"f83756d3-a139-4936-8fce-ab0bbc2a6ea3","value":true}
         *     ],
         *     "rendition":"/fotoweb/archives/5030-ACME's-product-catalog/orders/acme_earthquake.jpg.info/__renditions/1600px",
         *     "expires":"2018-02-20T11:12:32.570Z",
         *     "adminComment":""
         *   }]
         * }
         * 
         */
        public async Task<ActionResult> OrderConfirmed(OrderModel orderModel)
        {
            dynamic originalData = TempData["originalOrder"];
            string orderHref = (string)TempData["fullOrderHref"];
            string orderReference = originalData.orderReference;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(orderHref);
            request.Accept = @"application/vnd.fotoware.order+json";
            request.Headers["FWAPIToken"] = ApiKey;
            request.Method = "SUBMIT";

            // set expiry date of all items for SUBMIT request
            var jsonDataToSave = CreateJSONDataToSubmit(originalData);
            var json = JsonConvert.SerializeObject(jsonDataToSave);
            Byte[] byteArray = new UTF8Encoding().GetBytes(json);
            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    ViewBag.OrderReference = orderReference;
                    ViewBag.OrderHref = orderHref;
                    ViewBag.OrderHistoryViewHref = Host + "/fotoweb/views/orders";
                    return View();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
            }

            return null;
        }

        /*
         * Create JSON data to send in SUBMIT request.
         * Set expires to all order items to todays date + 60 days.
         */
        private ExpandoObject CreateJSONDataToSubmit(dynamic originalData)
        {
            var orderItemList = new List<ExpandoObject>();
            DynamicJsonArray orderItems = originalData.orderItems;
            for (var i = 0; i < orderItems.Count(); i++)
            {
                dynamic orderItem = new ExpandoObject();
                orderItem.itemData = orderItems[i].itemData;
                orderItem.rendition = orderItems[i].rendition.href;
                orderItem.expires = DateTime.UtcNow.AddDays(60).ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                orderItem.adminComment = "";
                orderItemList.Add(orderItem);
            }

            dynamic jsonDataToSave = new ExpandoObject();
            jsonDataToSave.orderData = originalData.orderData;
            jsonDataToSave.adminComment = "";
            jsonDataToSave.expires = null;
            jsonDataToSave.orderItems = orderItemList.ToArray();
            return jsonDataToSave;
        }
    }
}
