# README #

Example for integrating FotoWeb orders to a custom checkout page.

For more information, see [the order API documentation](https://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/01_The_FotoWeb_RESTful_API/Orders)


## Requirements ##

* Microsoft Visual Studio 2015
* A FotoWeb server
* An API key for the FotoWeb server

## How to run this example ##

* Open the solution (.sln) file in Microsoft Visual Studio 2015
* Change change the Host and ApiKey variables found in Controllers/OrderController.cs.
* Run the example
* Set custom checkout page in FotoWeb configuration to http://localhost:61025/Order?orderHref={{orderhref}}
* Add some assets to a shopping cart in FotoWeb main interface, go to cart page and checkout.

## How do I integrate this code into my own? ##

You are free to use this code in your own proprietary products. Please see LICENSE.txt for more details.